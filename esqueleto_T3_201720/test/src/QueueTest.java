package src;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;
import model.data_structures.IQueue;
import model.data_structures.Queue;

public class QueueTest<E> extends TestCase {

	
	Queue queue;
	
	public void setUp()
	{
		queue = new Queue();
	}
	
	//agregar un elemento, obtener un elemento, dar el tamaño
	public void testEnqueue()
	{
		setUp();
		
		queue.enqueue(1);
		queue.enqueue(3);
		queue.enqueue(2);
		assertEquals(false,!queue.isEmpty());
		assertEquals(3, queue.size());
	}
	public void testDequeue()
	{
		
		queue.enqueue(1);
		assertEquals(1,queue.dequeue());
		queue.enqueue(1);
		queue.enqueue(2);
		assertEquals(1, queue.dequeue());
	}

}
