package api;

import java.io.File;

import model.data_structures.IStack;
import model.exceptions.TripNotFoundException;
import model.vo.StopVO;

public interface ISTSManager {

	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	public void readBusUpdate(File rtFile);
	
	public IStack<StopVO> listStops (Integer tripID) throws TripNotFoundException;

	public void loadStops(String stopsFile);
}
