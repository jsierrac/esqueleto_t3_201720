package model.vo;

public class StopVO implements Comparable <StopVO> {

	private String id;
	private String code;
	private String name;
	private String desc;
	private String lat;
	private String lon;

	private String zoneId;

	private String locationType;


	public StopVO(String id, String code, String name, String desc, String lat, String lon, String zoneId, String locationType) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.desc = desc;
		this.lat = lat;
		this.lon = lon;
		this.zoneId = zoneId;
		this.locationType = locationType;
	}

	public StopVO(String stopName) {
		this.name = stopName;
	}



	public String id() {
		return id;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public String getId() {
		return id;
	}
	
	public double getLatitude() {
		
		return Double.parseDouble(lat);
	}
	
	public double getLongitude() {
		
		return Double.parseDouble(lon);
	}

	@Override
	public int compareTo(StopVO o) {
		return (this.getName()).compareTo(o.getName());
	}

	@Override
	public String toString() {
		return id + "";
	}


	
}
