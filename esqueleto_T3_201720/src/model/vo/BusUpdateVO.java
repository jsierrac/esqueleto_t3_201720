package model.vo;

public class BusUpdateVO {

	//--------------------------------------------------------------------------------------------------
	// Atributos
	//--------------------------------------------------------------------------------------------------
	
	private String VehicleNo;
	
	private int TripId;
	
	private String RouteNo;
	
	private String Direction;
	
	private String Destination;
	
	private String Pattern;
	
	private double Latitude;
	
	private double Longitude;
	
	private String RecordedTime;
	
	private RouteMap RouteMap;
	
	//--------------------------------------------------------------------------------------------------
	// Sub Clases
	//--------------------------------------------------------------------------------------------------
	
	private class RouteMap {
		
		private String href;
		
		public RouteMap( String pHref ) {
			
			href = pHref;
		}
	}
	
	//--------------------------------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------------------------------
	
	public BusUpdateVO( String pVehicleNo, String pTripId, String pRouteNo, String pDirection, String pDestination, String pPattern, String pLatitude, String pLongitud, String pRecordedTime, String pHref ) {

		VehicleNo = pVehicleNo;
		
		TripId = Integer.parseInt(pTripId);
		
		RouteNo = pRouteNo;
		
		Direction = pDirection;
		
		Destination = pDestination;
		
		Pattern = pPattern;
		
		Latitude = Double.parseDouble(pLatitude);
		
		Longitude = Double.parseDouble(pLongitud);
		
		RecordedTime = pRecordedTime;
		
		RouteMap = new RouteMap(pHref);
	}
	
	//--------------------------------------------------------------------------------------------------
	// Metodos
	//--------------------------------------------------------------------------------------------------
	
	public void setVehicleNO( String pVehicleNO ){
		
		VehicleNo = pVehicleNO;
	}
	
	public int getTripId() {
		
		return TripId;
	}
	
	public double getLatitude() {
		
		return Latitude;
	}
	
	public double getLongitude() {
		
		return Longitude;
	}
}
