package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Created by julianasierra on 27/08/17.
 */
public class VOZone implements Comparable<VOZone>
{
   private String id;
   private DoubleLinkedList<StopVO> stops;


    public VOZone(String id) {
        this.id = id;
        stops= new DoubleLinkedList<>();
    }

    public String getId() {
        return id;
    }


    public DoubleLinkedList<StopVO> getStops() {
        return stops;
    }

    public void setStops(DoubleLinkedList<StopVO> stops) {
        this.stops = stops;
    }

    @Override
    public int compareTo(VOZone o)
    {
        return this.id.compareTo(o.toString());
    }

    @Override
    public String toString() {
        return id+"";
    }
}
