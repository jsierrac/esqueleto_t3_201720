package model.data_structures;

import java.util.Iterator;

public class RingList<T extends Comparable<T>> extends List<T> {

    //-------------------------------------------------------------------
    // Sub-Clases
    //-------------------------------------------------------------------

    //TODO HECHO
    private class RingListIterator implements Iterator<T> {

        private Node<T> current = first;
        private int currentIndex = 0;

        @Override
        public boolean hasNext() {
            // TODO Auto-generated method stub
           return currentIndex < getSize();
        }

        @Override
        public T next() {
            // TODO Auto-generated method stub
            
        	if( current != null ){
        		
        		T next = current.getItem();
        		current = current.getNext();
        		currentIndex++;
        		return next;
        	}
        	else {
        		
        		return null;
        	}

        }
    }


    //-------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------

    public RingList() {

        super();
    }
    //-------------------------------------------------------------------
    // Metodos
    //-------------------------------------------------------------------

    @Override
    public void addAtEnd(T pItem) {
        // TODO HECHO Auto-generated method stub

        if (first == null) {

            first = new Node<T>(null, null, pItem);
            
            first.setNext(first);
            
            first.setPrevious(first);
            
        } else if (this.getSize() == 1) {

            Node<T> nuevo = new Node<T>(first, first, pItem);

            first.setNext(nuevo);

            first.setPrevious(nuevo);
            
        } else {

            Node<T> nuevo = new Node<T>(first, first.getPrevious(), pItem);

            first.getPrevious().setNext(nuevo);

            first.setPrevious(nuevo);
        }
        size++;
    }
    
    @Override
    public void add(T pItem) {
    	// TODO Auto-generated method stub

    	if(first == null ){

    		Node<T> nuevo = new Node<T>(null, null, pItem);

    		first = nuevo;

    		first.setNext(first);

    		first.setPrevious(first);
    	}
    	else {

    		Node<T> temp = first;
    		
    		int currentIndex = 0;

      		while( temp != null && temp.getItem().compareTo(pItem) <= 0 && currentIndex < size ){

      			temp = temp.getNext();
      			
      			currentIndex++;
    		}
      		
      		if( temp != null ){
      			
      			//Added at te middle
      			
      			Node<T> nuevo = new Node<T>(temp, temp.getPrevious(), pItem);
      			
      			temp.getPrevious().setNext(nuevo);
      			
      			temp.setPrevious(nuevo);
      		}
      		else {
      			
      			//Added at the end
      			
      			Node<T> nuevo = new Node<>(null, temp, pItem);
      			
      			first.getPrevious().setNext(nuevo);
      			
      			first.setPrevious(nuevo);
      		}
    	}
    	
    	size++;
    }

	@Override
	public void addAtK(int k, T pItem) {
		// TODO Auto-generated method stub
		Node act = first;
		
		if( k < size ){
			
			for( int i = 0; i < k; i++ ){
				
				act = act.getNext();
			}
			
			act = act.getPrevious();
			
			if( act.getNext() != first ){
				
				Node<T> nuevo = new Node<T>(act.getNext(), act, pItem);
				
				act.getNext().setPrevious(nuevo);
				
				act.setNext(nuevo);
			}
			else {
				
				Node<T> nuevo = new Node<T>(first, act, pItem);
				
				first.setPrevious(nuevo);
				
				act.setNext(nuevo);
			}
		}	
	}
	
    @Override
    public Iterator<T> iterator() {
        // TODO Auto-generated method stub
        return new RingListIterator();
    }

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
		if( actual != null ){
			
			if(actual.equals(first)) {

				if( size == 1 ){
					
					first = null;
				}
				else {
					
					Node<T> temp = first.getPrevious();
					
					first = first.getNext();
					
					first.setPrevious(temp);
					
					temp.setNext(first);
				}
				
				size --;
			}
			else {

				Node<T> temp = actual.getPrevious();

				actual.getPrevious().setNext(actual.getNext());

				actual.getNext().setPrevious(actual.getPrevious());

				actual = temp;
				
				size--;
			}
		}
	}

	@Override
	public void deleteAtK(int k) {
		// TODO Auto-generated method stub
		
		Node act = first;
		
		if( k < size ){
			
			for( int i = 0; i < k; i++ ){
				
				act = act.getNext();
			}
			
			actual = act;

			delete();
		}
		
	}


}
