package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	//-------------------------------------------------------------------
	// Metodos
	//-------------------------------------------------------------------
	
	void add(T pItem);
	
	void addAtEnd( T pItem );
	
	void addAtK( int k, T pItem );
	
	T getElement( int pPosicion );
	
	Integer getSize();
	
	void delete();
	
	void deleteAtK( int k );
	
	void next();
	
	void previous();	
	
	void actualFirst();
	

}
