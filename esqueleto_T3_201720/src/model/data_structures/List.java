package model.data_structures;

import java.util.Iterator;

public abstract class List<T extends Comparable<T>> implements IList<T> {
	
	//-------------------------------------------------------------------
	// Atributos
	//-------------------------------------------------------------------

	protected int size;

	protected Node<T> first;
	
	protected Node<T> actual;

	//-------------------------------------------------------------------
	// Constructor
	//-------------------------------------------------------------------
	
	public List(){
		
		first = null;
		
		actual = first;
	}

//	@Override
//	public void addAtK( int k, T pItem ) {
//		// TODO HECHO Auto-generated method stub
//		
//		Iterator<T> iter = this.iterator();
//		
//		if( this.getSize()>= k+1 ){
//			
//			for( int i = 0; i < k; i++ ){
//				
//				actual = (Node<T>) iter.next();
//			}
//			
//			Node nuevo = new Node<T>(actual, actual.getPrevious(), pItem);
//			
//			actual.getPrevious().setNext(nuevo);
//			
//			actual.setPrevious(nuevo);
//		}
//
//		size++;
//		actual = first;
//	}
//
//	@Override
//	public void delete() {
//		// TODO HECHO Auto-generated method stub
//		
//		actual.getPrevious().setNext(actual.getNext());
//		
//		actual.getNext().setPrevious(actual);
//		size--;
//	}
//	
//	@Override
//	public void deleteAtK( int k ) {
//		// TODO HECHO Auto-generated method stub
//		
//		Iterator<T> iter = this.iterator();
//		
//		if( this.getSize()>= k+1 ){
//			
//			for( int i = 0; i < k; i++ ){
//				
//				actual = (Node<T>) iter.next();
//			}
//			
//			delete();
//		}
//		
//		actual = first;
//		size--;
//	}
	
	@Override
	public Integer getSize() {
		// TODO HECHO Auto-generated method stub
		
		return size;
	}
	
	@Override
	public T getElement( int pPosicion ) {
		// TODO HECHO Auto-generated method stub
		
		Node act = first;
		
		if( pPosicion < size ){
			
			for( int i = 0; i < pPosicion; i++ ){
				
				act = act.getNext();
			}

			return (T) act.getItem();
		}
		else {
			
			return null;
		}
	}
	
	@Override
	public void next() {
		// TODO HECHO Auto-generated method stub
		
		actual = actual.getNext();
	}
	
	@Override
	public void previous() {
		// TODO HECHO Auto-generated method stub
	
		actual = actual.getPrevious();
	}
	

    public T find(T elem) {
		T resp = null;

		Iterator<T> iter = this.iterator();
		while (iter.hasNext() && resp == null) {
			T currentElement = iter.next();
			if(currentElement.compareTo(elem) == 0) {
				resp = currentElement;
			}
		}

		return resp;
    }
    
    public void actualFirst() {
    	
    	actual = first;
    }

}
