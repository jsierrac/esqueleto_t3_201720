package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by julianasierra on 31/08/17.
 */
public class Stack<E> implements IStack<E> {


    private Node<E> first;
    private int n;

    public boolean isEmpty() {
        return n == 0;
    }

    public int size()

    {
        return n;
    }

    @Override
    public void push(E item) {
        Node oldFirst = first;
        Node newFirst = new Node<E>(item);

        if (isEmpty()) {
            first = newFirst;
            n++;
        } else {

            newFirst.setNext(oldFirst);
           first=newFirst;
            n++;

        }
    }

    @Override
    public E pop() {
        if(isEmpty()) {
            return null;
        }
        else{

            Node<E> oldFist=first;
            first=oldFist.next;
            n--;

            return oldFist.item;
        }
    }

    private class Node<E> {

        private Node next;

        private E item;


        private Node(E pItem) {
            item = pItem;
        }

        private void setItem(E item) {
            this.item = item;
        }

        private Node getNext() {
            return next;
        }

        private void setNext(Node next) {
            this.next = next;
        }



    }

    private class stackIterator implements Iterator<E> {
        private Node<E> current;

        public stackIterator() {
            current = first;

        }


        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            E item = current.item;
            current = current.next;
            return item;

        }
    }
    public stackIterator iterator()
    {
        return new stackIterator();
    }
}
