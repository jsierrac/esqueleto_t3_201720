package model.data_structures;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by julianasierra on 31/08/17.
 */
public class Queue<E> implements IQueue<E>{

	private Node<E> first;
	private Node<E> last;
	private int n;

	public Queue() {
		first = null;
		last = null;
	}


	public boolean isEmpty() {
		return n == 0;
	}

	public int size() {
		return n;
	}

	public E peek() throws Exception {
		if (isEmpty()) {
			throw new NoSuchMethodException();
		} else {
			return first.item;
		}
	}


	@java.lang.Override
	public void enqueue(E item) {
		Node<E> oldLast = last;
		last= new Node<E>(item);
		last.next=null;


		if (isEmpty()) {
			first = last;
			
		} else {
			oldLast.setNext(last);
		}
		n++;
	}

	@java.lang.Override
	public E dequeue() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		E item = first.item;
		first = first.getNext();
		n--;
		if (isEmpty()) {
			last = null;
		}
		return item;
	}


	private class Node<E> {

		private Node<E> next;
		private E item;

		private Node(E pItem) {
			item = pItem;
		}

		private void setNext(Node<E> next) {
			this.next = next;
		}

		private Node<E> getNext() {
			return next;
		}
	}
	private class QueueIterator implements Iterator<E> {
		
		private Node<E> current = first;

		@Override
		public boolean hasNext() {
			return current != null;
		}

		@Override
		public E next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			E item = current.item;
			current = current.next;
			return item;

		}
	}
	
	public Iterator<E> iterator()
	{
		return new QueueIterator();
	}
}
