package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T extends Comparable<T>>  extends List<T>
{
	
	//-------------------------------------------------------------------
	// Atributos
	//-------------------------------------------------------------------
	
	private Node<T> last;
	
	//-------------------------------------------------------------------
	// Sub-Clases
	//-------------------------------------------------------------------
	
	private class DoubleLinkedListIterator implements Iterator<T> {

		private Node<T> current = first;

		@Override
		public boolean hasNext() {
			// TODO HECHO Auto-generated method stub
			
			return current != null;
		}

		@Override
		public T next() {
			// TODO HECHO Auto-generated method stub
			if(current != null){
				
				T next = current.getItem();
				current = current.getNext();
				return next;
			}
			else {
				
				return null;
			}
		}
	}
	
	//-------------------------------------------------------------------
	// Constructor
	//-------------------------------------------------------------------
	
	public DoubleLinkedList() {
		
		super();
		
		last = null;
	}

	//-------------------------------------------------------------------
	// Metodos
	//-------------------------------------------------------------------


    @Override
    public void add(T pItem){
    	
    	if(first == null ){
    		
    		Node<T> nuevo = new Node<T>(null, null, pItem);
    		
    		first = nuevo;
    		
    		last = nuevo;
    	}
    	else {

    		if(first.getItem().compareTo(pItem) >= 0) {
    			//Added at the first position
				Node<T> nuevo = new Node<T>(first, null, pItem);
				first.setPrevious(nuevo);
				first = nuevo;
			} else {

				Node<T> temp = first;

				while ( temp != null && temp.getItem().compareTo(pItem) <= 0 ) {
					
					temp = temp.getNext();
				}

				if (temp != null) {
					//Added in the middle
					Node<T> nuevo = new Node<T>(temp, temp.getPrevious(), pItem);
					temp.getPrevious().setNext(nuevo);
					temp.setPrevious(nuevo);
				} else {
					//Added at the end
					Node<T> nuevo = new Node<T>(null, last, pItem);
					last.setNext(nuevo);
					last = nuevo;
				}
			}
    	}
    	
    	size++;
    }

    @Override
	public void addAtEnd(T pItem) {
		// TODO HECHO Auto-generated method stub
		
		if( first == null ){
			
			Node<T> nuevo = new Node<T>(null, null, pItem);		
			
			first = nuevo;
			
			last = nuevo;
			
		} else {

			
			Node<T> nuevo = new Node<T>(null, last, pItem);
			
			last.setNext(nuevo);
			last = nuevo;
		}
		size++;
	}
    
	@Override
	public void addAtK(int k, T pItem) {
		// TODO Auto-generated method stub
		
		Node act = first;
		
		if( k < size ){
			
			for( int i = 0; i < k; i++ ){
				
				act = act.getNext();
			}
			
			act = act.getPrevious();
			
			if( act.getNext() != null ){
				
				Node<T> nuevo = new Node<T>(act.getNext(), act, pItem);
				
				act.getNext().setPrevious(nuevo);
				
				act.setNext(nuevo);
			}
			else {
				
				Node<T> nuevo = new Node<T>(null, act, pItem);
				
				act.setNext(nuevo);
			}
		}		
	}

	@Override
	public Iterator<T> iterator() {
		// TODO HECHO Auto-generated method stub
		return new DoubleLinkedListIterator();
	}

	public T getLastElement() {

		return last.getItem();
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
		if( actual != null ){
			
			if(actual.equals(first)) {

				first = first.getNext();
				
				try {
					
					first.setPrevious(null);
				}
				catch(Exception e){
					
				}
				size--;
			}
			else {

				Node<T> temp = actual.getPrevious();

				actual.getPrevious().setNext(actual.getNext());

				if(actual.getNext() != null){

					actual.getNext().setPrevious(actual.getPrevious());
				}

				actual = temp;
				
				size--;
			}
		}
	}

	@Override
	public void deleteAtK(int k) {
		// TODO Auto-generated method stub
		
		Node act = first;
		
		if( k < size ){
			
			for( int i = 0; i < k; i++ ){
				
				act = act.getNext();
			}

			act = act.getPrevious();

			actual = act;

			delete();
		}
	}
}
