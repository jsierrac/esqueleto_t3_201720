package model.data_structures;

public class Node<T>
{
	
	//-------------------------------------------------------------------
	// Atributos
	//-------------------------------------------------------------------
	
	private Node<T> next;
	
	private Node<T> previous;
	
	private T item;
	
	//-------------------------------------------------------------------
	// Constructor
	//-------------------------------------------------------------------
	
	public Node( Node<T> pNext, Node<T> pPrevious, T pItem )
	{
		
		next = pNext;
		
		previous = pPrevious;
		
		item = pItem;
	}
	
	//-------------------------------------------------------------------
	// Metodos
	//-------------------------------------------------------------------
	
	public Node<T> getNext() {
		
		return next;
	}
	
	public Node<T> getPrevious() {
		
		return previous;
	}
	
	public T getItem() {
		
		return item;
	}
	
	public void setNext( Node<T> pNext ) {
		
		 next = pNext;
	}
	
	public void setPrevious( Node<T> pPrevious ) {
		
		previous = pPrevious;
	}
	
	public void setItem( T pItem ) {
		
		item = pItem;
	}

	@Override
	public String toString() {
		return "Node("+item.toString()+")";
	}
}
