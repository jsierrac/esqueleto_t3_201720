package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Stack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.exceptions.TripNotFoundException;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import model.vo.VOZone;
import api.ISTSManager;

public class STSManager implements ISTSManager{

	//--------------------------------------------------------------------------------------------------
	// Atributos
	//--------------------------------------------------------------------------------------------------
	
	Queue colaBuses = new Queue();
	
    private RingList<VOZone> zones;
	
	//--------------------------------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------------------------------
    
	//--------------------------------------------------------------------------------------------------
	// Metodos
	//--------------------------------------------------------------------------------------------------
	
	@Override
	public void readBusUpdate(File rtFile) {
		// TODO HECHOS Auto-generated method stub
	    
		BufferedReader reader = null;
	    
		try {
	        reader = new BufferedReader(new FileReader(rtFile));
	        
	        Gson gson = new GsonBuilder().create();
	        
	        BusUpdateVO[] buses = gson.fromJson(reader, BusUpdateVO[].class);
	        
	        for( int i = 0; i < buses.length; i++ ) {
	        	
	        		
	        		colaBuses.enqueue(buses[i]);
	        		
	        		System.out.println(buses[i].getTripId());
	        }

	    } catch (FileNotFoundException e) {
	        
	    		e.printStackTrace();
	    }
	}

	@Override
	public IStack<StopVO> listStops(Integer tripID) throws TripNotFoundException {
		// TODO Auto-generated method stub
		
		model.data_structures.Stack<StopVO> pilaStops = new model.data_structures.Stack<StopVO>();
		
		BusUpdateVO bus = null;
		
		Iterator iterBuses = colaBuses.iterator();
		
		BusUpdateVO busActual = (BusUpdateVO) iterBuses.next();
		
		while( iterBuses.hasNext() && bus == null ){
			
			if( busActual.getTripId() == tripID ){
				
				bus = busActual;
			}
			
			busActual = (BusUpdateVO) iterBuses.next();
		}
		
		if( bus == null ){
			
			throw new TripNotFoundException();
		}
		
		Iterator iterZones = zones.iterator();
		
		while( iterZones.hasNext() ) {
			
			VOZone zoneActual = (VOZone) iterZones.next();
			
			Iterator iterStops = zoneActual.getStops().iterator();
			
			while( iterStops.hasNext() ) {
				
				StopVO stopActual = (StopVO) iterStops.next();
				
				double distance = this.getDistance(bus.getLatitude(), bus.getLongitude(), stopActual.getLatitude(), stopActual.getLongitude());
				
				if( distance <= 70 ){
					
					pilaStops.push(stopActual);
				}
				
//				stopActual = (StopVO) iterStops.next();
			}
			
//			zoneActual = (VOZone) iterZones.next();
		}
		
		return (IStack<StopVO>) pilaStops;
	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		
        try {

            BufferedReader in = new BufferedReader(new FileReader(stopsFile));
            String line = in.readLine();
            line = in.readLine();

            zones = new RingList<>();

            while (line != null) {
                String[] values = line.split(",");
                VOZone newZone = new VOZone(values[6]);
                VOZone stopZone = zones.find(newZone);
                StopVO newStop = new StopVO(values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[8]);
                if (stopZone == null) {
                    newZone.getStops().add(newStop);
                    zones.add(newZone);
                } else {
                    stopZone.getStops().add(newStop);
                }
                line = in.readLine();
            }


            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        // TODO Auto-generated method stub
        final int R = 6371*1000; // Radious of the earth
    
        Double latDistance = Math.toRadians(lat2-lat1);
        Double lonDistance = Math.toRadians(lon2-lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double distance = R * c;
         
        return distance;

	}

	private Double toRad(double d) {
		// TODO Auto-generated method stub
		return d * Math.PI / 180;
	}


}
